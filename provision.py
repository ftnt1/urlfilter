#!/usr/bin/python

from ftntlib import fortimanager
#import fortimanager
import argparse

parser = argparse.ArgumentParser(description='Script to update a Web Filter URL objects with URLs from a CSV file')
parser.add_argument('-i', '--ip', help="FortiManager's IP", required=True)
parser.add_argument('-l', '--username', help="FortiManager's Username", required=True)
parser.add_argument('-p', '--password', help="FortiManager's Password", required=False)
parser.add_argument('-f', '--file', help="CSV file describing the URLs to consider", required=True)
args = parser.parse_args()

ip = args.ip
username = args.username
password = ''
if args.password:
    password = args.password
file = args.file

fmg = fortimanager.FMG()

print('Connecting to %s with %s' % (ip, username))
fmg.login(ip, username, password)

print('Processing file %s' % file)
f = open(file)
for line in f:
    line = line.rstrip()

    print("Line [%s]" % line)
    (adom, urlfilter, urlf_url, urlf_type, urlf_action, urlf_status, urlf_referrer) = line.split(',')

    print("Does ADOM %s exist..." % adom)
    if fmg.DoesAdomExist(adom):
        print("ADOM %s exist! Continuing..." % adom)

        print("Does URL Filter %s/%s exist..." % (adom, urlfilter))
        if fmg.DoesUrlFilterExist(adom, urlfilter):
            print("URL Filter %s/%s does exist! Continuing..." % (adom, urlfilter))

            fmg.LoadUrlFilter(adom, urlfilter)

            print("Does %s/%s/%s URL entry exist..." % (adom, urlfilter, urlf_url))
            if fmg.DoesUrlFilterEntryExist(adom, urlfilter, urlf_url):
                print("%s/%s/%s URL entry does exist! Skipping this line" % (adom, urlfilter, urlf_url))
                next
            else:
                print("%s/%s/%s URL entry doesn't exist! Continuing..." % (adom, urlfilter, urlf_url))
                print("Adding URL entry %s in URL Filter %s/%s" % (urlf_url, adom, urlfilter))
                fmg.AddUrlFilterEntry(adom, urlfilter, urlf_url, urlf_type, urlf_action, urlf_status, urlf_referrer)

        else:
            print("URL Filter %s/%s doesn't exist! Skipping this line" % (adom, urlfilter))
            next

    else:
        print("ADOM %s doesn't exist! Skipping this line" % adom)
        next

print('Disconnecting from %s' % ip)
fmg.logout()
